-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Czas generowania: 09 Kwi 2018, 14:37
-- Wersja serwera: 10.1.28-MariaDB
-- Wersja PHP: 5.6.32

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Baza danych: `rpg_game`
--
CREATE DATABASE IF NOT EXISTS `rpg_game` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;
USE `rpg_game`;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `abilities`
--

DROP TABLE IF EXISTS `abilities`;
CREATE TABLE `abilities` (
  `aid` int(11) NOT NULL COMMENT 'Liczba porządkowa',
  `name` int(11) NOT NULL COMMENT 'Nazwa umiejętności',
  `description` int(11) NOT NULL COMMENT 'Opis umiejętności'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='Zawiera umiejętności dostępne dla graczy';

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `abilities_disqualifications`
--

DROP TABLE IF EXISTS `abilities_disqualifications`;
CREATE TABLE `abilities_disqualifications` (
  `ability` int(11) NOT NULL COMMENT 'Umiejętność (identyfiaktor)',
  `ability_disqualification` int(11) NOT NULL COMMENT 'Identyfikator umiejętności, która dyskwalifikuje naukę umiejętności'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='kontrola nad blokowaniem umiejętności (wiele-wiele)';

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `abilities_effects`
--

DROP TABLE IF EXISTS `abilities_effects`;
CREATE TABLE `abilities_effects` (
  `ability` int(11) NOT NULL COMMENT 'Identyfikator umiejętności',
  `effect` int(11) NOT NULL COMMENT 'Identyfikator efektu'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='Relacja wiele-wiele łącząca ze sobą umiejętności i efekty';

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `abilities_requirements`
--

DROP TABLE IF EXISTS `abilities_requirements`;
CREATE TABLE `abilities_requirements` (
  `ability` int(11) NOT NULL COMMENT 'Umiejętność do zdobycia',
  `require_ability` int(11) NOT NULL COMMENT 'Identyfikator umiejętności do oblokowania umiejętności z pola ability'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='kontrola nad wymaganiami umiejętności (wiele-wiele)';

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `academies_regions`
--

DROP TABLE IF EXISTS `academies_regions`;
CREATE TABLE `academies_regions` (
  `academy` int(11) NOT NULL COMMENT 'identyfikator akademii (z tabeli budynki)',
  `region` int(11) NOT NULL COMMENT 'Identyfikator regionu'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='Zawiera połączenie akademii do regionu';

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `avatars`
--

DROP TABLE IF EXISTS `avatars`;
CREATE TABLE `avatars` (
  `aid` int(11) NOT NULL COMMENT 'Identyfikator postaci',
  `avatar_name` text NOT NULL COMMENT 'Imię postaci',
  `avatarBio` varchar(11) NOT NULL COMMENT 'Biografia',
  `avatarace` int(11) NOT NULL COMMENT 'Umiejętność specjalna',
  `strength` int(11) NOT NULL COMMENT 'Siła',
  `accuracy` int(11) NOT NULL COMMENT 'Zręczność',
  `spellpower` int(11) NOT NULL COMMENT 'Siła czarów',
  `weight` int(11) NOT NULL COMMENT 'Tężyzna',
  `knowledge` int(11) NOT NULL COMMENT 'Wiedza',
  `intelligence` int(11) NOT NULL COMMENT 'inteligencja',
  `willpower` int(11) NOT NULL COMMENT 'Siła woli',
  `race` text NOT NULL COMMENT 'Rasa/podrasa',
  `gender` tinyint(1) NOT NULL COMMENT 'Płeć',
  `prefgod` text NOT NULL COMMENT 'Bóstwo',
  `prefNature` int(11) NOT NULL COMMENT 'Charakter',
  `pic` varchar(11) NOT NULL COMMENT 'Obraz'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='Tabela zawierająca karty postaci - zarówno gracza jak i NPC';

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `buildings`
--

DROP TABLE IF EXISTS `buildings`;
CREATE TABLE `buildings` (
  `building` int(11) NOT NULL COMMENT 'Liczba porządkowa tabeli',
  `kind` int(11) NOT NULL COMMENT 'rodzaj budynku (hala, domek jednorodzinny, zamek, szałas itp.)',
  `type` int(11) NOT NULL COMMENT 'Typ budynku (np. akademia, sklep, mieszkanie, kuźnia itp.)',
  `localization` int(11) NOT NULL COMMENT 'Lokalizacja budynku (obrzeża miasta, centrum, konkretna dzielnica)',
  `town` int(11) NOT NULL COMMENT 'Identyfikator miasta',
  `owner` int(11) NOT NULL COMMENT 'Właściciel budynku (identyfikator do kart postaci)',
  `description` int(11) NOT NULL COMMENT 'Opis budynku (opcjallne)'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='Zawiera wszystkie budynki w grze';

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `effects`
--

DROP TABLE IF EXISTS `effects`;
CREATE TABLE `effects` (
  `eid` int(11) NOT NULL COMMENT 'Identyfikator w tabeli',
  `name` text NOT NULL COMMENT 'Nazwa efektu',
  `type` int(11) NOT NULL COMMENT 'Typ (pozytywny/negatywny)',
  `power` int(11) NOT NULL COMMENT 'Liczbowa moc efektu',
  `powerType` int(11) NOT NULL COMMENT 'Typ mocy efektu (całkowity, procentowy, na poziom itp.)'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='Zawiera efekty umiejętności';

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `emailservers`
--

DROP TABLE IF EXISTS `emailservers`;
CREATE TABLE `emailservers` (
  `eid` int(11) NOT NULL COMMENT 'Identyfikator serwera poczty',
  `postfix` text NOT NULL COMMENT 'Domenowy adres serwera e-poczty (np. gmail.com, mojapoczta.pl itd.)'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='Tabela zawiera nazwy serwrów poczty elektornicznej (bez zbędnego powtarzania nazw przy poczcie)';

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `equipments`
--

DROP TABLE IF EXISTS `equipments`;
CREATE TABLE `equipments` (
  `id` int(11) NOT NULL COMMENT 'Id kolejnej pozycji ekwipunku',
  `player` int(11) NOT NULL COMMENT 'Gracz (id), dla którego przypisany jest przedmiot',
  `player_part` int(11) NOT NULL COMMENT 'Miejsce w ekwipunku (moze być np. tors, głowa, szyja, nogi jak i sakwa) ',
  `item` int(11) NOT NULL COMMENT 'Identyfikator przedmiotu w ekwipunku'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='Ekpiunek wszystkich graczy (zapisywany w jednej tabeli)';

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `gods`
--

DROP TABLE IF EXISTS `gods`;
CREATE TABLE `gods` (
  `id` int(11) NOT NULL COMMENT 'Liczba porządkowa w tabeli',
  `god_name` varchar(100) NOT NULL COMMENT 'Imię boga/bogini',
  `gifts` text NOT NULL COMMENT 'Dary (pozytywne efekty dla wyznawcy)',
  `curses` text NOT NULL COMMENT 'Przekleństwa (negatywne efekty dla wroga wyznawcy',
  `nature` varchar(20) NOT NULL COMMENT 'Charakter'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='Zawiera wszystkie bóstwa w grze (oraz pozycję ateistyczną)';

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `items`
--

DROP TABLE IF EXISTS `items`;
CREATE TABLE `items` (
  `id` int(11) NOT NULL COMMENT 'Identyfikator przedmiotu',
  `name` text NOT NULL COMMENT 'Nazwa przedmiotu',
  `effect` text NOT NULL COMMENT 'Efekt przedmiotu',
  `useField` int(11) NOT NULL COMMENT 'Wskazuje, w któej częście ekwpipunku osobistego może być użyty przedmiot (np. szyja, tors, głowa)',
  `description` text NOT NULL COMMENT 'Opis przedmiotu (opcjonalny)'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='Zawiera dostępne dla graczy i niezależnych przedmioty';

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `npcroles`
--

DROP TABLE IF EXISTS `npcroles`;
CREATE TABLE `npcroles` (
  `id` int(11) NOT NULL COMMENT 'Identyfikator postaci niezależnej',
  `nature` int(11) NOT NULL COMMENT 'Charakter',
  `avid` int(11) NOT NULL COMMENT 'Identyfikator odnoszący się do karty postaci',
  `moodtoothers` varchar(11) NOT NULL COMMENT 'Nastawienie do innych (innych niż gracz, np. agresywne, handlowe itp.))',
  `role_in_game` text NOT NULL COMMENT 'Rola odgrywana w rozgrywce (np. handlarz, zabójca, przewodnik)'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='Tabela zawierająca rolę poszczeólnych NPC (wraz z odnośnikiem do karty samej postaci -> tabela avatars)';

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `regions`
--

DROP TABLE IF EXISTS `regions`;
CREATE TABLE `regions` (
  `region_id` int(10) UNSIGNED DEFAULT NULL COMMENT 'Identyfikator regionu',
  `currentSuvereign` int(11) DEFAULT NULL COMMENT 'Aktualny władca krainy',
  `dominatingCult` int(11) DEFAULT NULL COMMENT 'Dominujący kult bóstwa'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='Tabela zawiera informacje dotyczące regionów w grze';

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `uid` int(11) NOT NULL COMMENT 'Identyfikator użytkownika',
  `name` int(11) NOT NULL COMMENT 'Imię',
  `second_name` int(11) NOT NULL COMMENT 'Drugie imię (opcjonalne)',
  `surname` text NOT NULL COMMENT 'Nazwisko',
  `emailName` char(20) NOT NULL COMMENT 'Adres poczty (zachowywane bez serwera!)',
  `pass` int(11) NOT NULL COMMENT 'Hasło użytkownika (najlepiej zapisywać poprzez funkcję SHA2)',
  `ingame_name` int(11) NOT NULL COMMENT 'Nazwa w grze (login/nick)',
  `create_date` int(11) NOT NULL COMMENT 'Data dołączenia',
  `privilege` int(1) NOT NULL COMMENT 'Uprawnienia(obecnie możliwe Administrator, Mistrz gry, Pomocnik, Gracz)'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='Pełne dane wszystkich graczy (luidzkich) naszego RPG (w tym dane mistrzów gry)';

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `usersroles`
--

DROP TABLE IF EXISTS `usersroles`;
CREATE TABLE `usersroles` (
  `user_id` text NOT NULL COMMENT 'Identyfikator użytkownika',
  `avatar_id` text NOT NULL COMMENT 'identyfikator karty postaci'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='Relacja wiele-wiele łącząca graczy z ich postaciami w grze';

--
-- Indeksy dla zrzutów tabel
--

--
-- Indexes for table `regions`
--
ALTER TABLE `regions`
  ADD UNIQUE KEY `region_id` (`region_id`),
  ADD UNIQUE KEY `dominatingCult` (`dominatingCult`);

--
-- Indexes for table `usersroles`
--
ALTER TABLE `usersroles`
  ADD PRIMARY KEY (`user_id`(20),`avatar_id`(20)) USING BTREE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
